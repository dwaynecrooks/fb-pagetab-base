FbPagetabBase::Application.routes.draw do
  resources :facebook, only: :create

  root to: 'welcome#index'

  controller :pages do
    get 'privacy_policy'
    get 'terms_of_service'
  end
end
