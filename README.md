# Facebook Page Tab Base Application

Need help developing a [Facebook Page Tab Application](https://developers.facebook.com/docs/appsonfacebook/pagetabs/)? Start here with this barebones app. It's built with [Ruby on Rails](http://rubyonrails.org/).

## Getting Up and Running with Local Development

**Step 1**

[Fork](https://confluence.atlassian.com/display/BITBUCKET/Forking+a+Repository) this repository. Then, [clone](https://confluence.atlassian.com/display/BITBUCKET/Clone+a+repository) it to work on the app locally.

**Step 2**

Setup the local development environment. The requirements include:

- [Git](http://git-scm.com/)
- [RVM](https://rvm.io/)
- [Ruby](https://www.ruby-lang.org/en/)
- [Heroku client](https://toolbelt.heroku.com/)
- [Foreman](http://ddollar.github.io/foreman/)
- [PostgreSQL](http://www.postgresql.org/)

Open `.ruby-gemset` and replace the current gemset name `fb-pagetab-base` with the name of the gemset you want to use for your project.

You'd also need a self-signed SSL certificate for running the app locally over HTTPS.

**Creating a self-signed SSL certificate:**

    $ mkdir ssl && cd ssl

    # Generate private key and certificate signing request

    $ openssl genrsa -des3 -out server.orig.key 2048
    $ openssl rsa -in server.orig.key -out server.key
    $ openssl req -new -key server.key -out server.csr

    # Generate SSL certificate

    $ openssl x509 -req -days 365 -in server.csr -signkey server.key -out server.crt

**N.B.** See [here](https://devcenter.heroku.com/articles/ssl-certificate-self) for details.

**Step 3**

Create an application on Facebook for local development.

Recommended settings:

- **Page Tab Name:** Anything you want
- **Page Tab URL:** http://localhost:5000/facebook
- **Secure Page Tab URL:** https://localhost:5000/facebook
- **Page Tab Admin Control:** On
- **Wide Page Tab?:** Yes

See [here](https://developers.facebook.com/docs/appsonfacebook/pagetabs/) for details.

**IMPORTANT:** Take note of the Facebook Application ID and Secret.

**Step 4**

Copy the example application configuration:

    $ mv config/application.example.yml config/applicaton.yml

Then, open `config/application.yml` and set the values of `FACEBOOK_APP_ID` and `FACEBOOK_APP_SECRET`.

**Step 5**

Add the application to a page.

Bring up the [Add to Page Dialog](https://developers.facebook.com/docs/reference/dialogs/add_to_page/) by navigating to the following endpoint:

    https://www.facebook.com/dialog/pagetab?app_id=FACEBOOK_APP_ID&next=https://www.facebook.com/

**TODO:** *Create a rake task to perform this step.*

**Step 6**

Run `bundle install --without production` to install the dependencies.

**Step 7**

Rename the application from `FbPagetabBase` to your application's name. A find and replace works well here. Open `app/views/layouts/application.html.erb` and change the page's title as well.

**Step 8**

Configure the database.

    $ bundle exec rake db:create
    $ bundle exec rake db:migrate

Configure the test database.

    $ bundle exec rake db:test:clone

**Step 9**

Run the tests.

    $ bundle exec rspec

**Step 9**

Assuming all the tests passed, start the application.

    $ foreman start

## Deploying to Heroku

**Step 1**

Create an application on Heroku.

    $ heroku create [app_name] --remote staging

**Step 2**

Deploy to Heroku with `git push`.

    $ git push staging master
    # or
    $ git push staging [branch_name]:master # to deply a specific branch
    # or
    $ git push staging HEAD:master # to deploy the current branch
    # or
    $ git push staging +[branch_name | HEAD]:master # to force a deploy of a specific branch or the current branch

**Step 3**

Run migrations.

    $ heroku run rake db:migrate

**Step 4**

Set environment variables.

    $ bundle exec rake figaro:heroku[app-name]

**Step 5**

Create an application on Facebook for the Heroku app and add the application to a page.

## Resources

- [Facebook Developers](https://developers.facebook.com/)
- [Facebook Developer Tools](https://developers.facebook.com/tools/)
- [Postman - REST Client](https://chrome.google.com/webstore/detail/postman-rest-client/fdmmgilgnpjigdojojpjoooidkmcomcm?hl=en)
- [Ruby on Rails Guides](http://guides.rubyonrails.org/)
- [Ruby on Rails API](http://api.rubyonrails.org/)
- [Bootstrap](http://getbootstrap.com/)
- [Sass](http://sass-lang.com/)
- [Compass](http://compass-style.org/)
- [jQuery](http://jquery.com/)
- [Bundler](http://bundler.io/)
- [Foreman](http://ddollar.github.io/foreman/)
- [How Heroku Works](https://devcenter.heroku.com/articles/how-heroku-works)
- [Getting Started with Ruby on Heroku](https://devcenter.heroku.com/articles/getting-started-with-ruby)
- [Getting Started with Rails 4.x on Heroku](https://devcenter.heroku.com/articles/getting-started-with-rails4)
