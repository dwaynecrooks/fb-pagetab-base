class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :user_id, :access_token, :first_name, :last_name, :email, null: false
      t.timestamps
    end

    add_index :users, :user_id, unique: true
  end
end
