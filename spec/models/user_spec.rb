require 'spec_helper'

describe User do

  subject(:user) { FactoryGirl.create(:user) }

  it { should be_valid }

  it { should validate_presence_of(:user_id) }
  it { should validate_presence_of(:access_token) }
  it { should validate_presence_of(:first_name) }
  it { should validate_presence_of(:last_name) }
  it { should validate_presence_of(:email) }

  it { should allow_value('foo@bar.com').for(:email) }
  it { should_not allow_value('foo').for(:email) }
  it { should_not allow_value('foo@bar').for(:email) }
  it { should_not allow_value('foo.bar').for(:email) }
end
