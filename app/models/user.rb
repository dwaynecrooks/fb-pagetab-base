class User < ActiveRecord::Base
  validates_presence_of :user_id, :access_token, :first_name, :last_name, :email
  validates :email, format: { with: /\A\S+@\S+\.\S+\z/ }

  def full_name
    "#{first_name} #{last_name}".strip
  end

  def profile_picture_url(width = 150, height = 150)
    graph.get_picture('me', width: width, height: height)
  end

  def friends
    # Returns an array of hashes in the format
    # { 'name' => '', 'id' => '' }
    @friends ||= graph.get_connection('me', 'friends').sort do |a, b|
      a['name'] <=> b['name']
    end
  end

  private

    def graph
      @graph ||= Koala::Facebook::API.new(access_token)
    end
end
