class UserService

  def initialize(user_id, oauth_token)
    @user_id = user_id
    @oauth_token = oauth_token
  end

  def update_or_create
    user = update_or_create!
  rescue => e
    Rails.logger.info e.message
    Rails.logger.info e.backtrace
  ensure
    user
  end

  private

    attr_accessor :user_id, :oauth_token

    def update_or_create!
      user = User.find_by(user_id: user_id)

      if user
        if user.access_token != oauth_token
          user.update!(access_token: oauth_token)
        end
      else
        graph = Koala::Facebook::API.new(oauth_token)
        profile = graph.get_object('me')

        user = User.create!(
          user_id: user_id,
          access_token: oauth_token,
          first_name: profile['first_name'],
          last_name: profile['last_name'],
          email: profile['email']
        )
      end
    end
end
