class FacebookController < ApplicationController

  helper_method :current_user

  skip_before_action :verify_authenticity_token, only: :create

  before_action :require_authentication, except: :create
  before_action :require_like, except: :create if ENV.key?('FAN_GATE')

  # The maximum number of times to allow the
  # user to attempt to login in one session
  MAX_LOGIN_ATTEMPTS = 2

  def create
    parsed_signed_request = request.env['facebook.parsed_signed_request']

    # Track whether or not the user likes the page
    session[:like] = parsed_signed_request['page']['liked']

    if parsed_signed_request.key?('user_id')
      # Your app is authorized
      # See https://developers.facebook.com/docs/appsonfacebook/tutorial/#auth

      user_id = parsed_signed_request['user_id']

      if UserService.new(user_id, parsed_signed_request['oauth_token']).update_or_create
        session[:user_id] = user_id
        redirect_to root_url
      else
        render status: :internal_server_error, text: "Sorry for the inconvenience! Your user account could not be accessed at this time. Please try again at a later time. If the problem persists then you can try contacting the page's administrator for immediate assistance."
      end
    elsif session[:login_attempts] == MAX_LOGIN_ATTEMPTS
      # For some reason params[:error_reason] was not being set to 'user_denied'
      # so I decided to track the number of login_attempts instead
      # See https://developers.facebook.com/docs/facebook-login/manually-build-a-login-flow#nonjscancel
      session[:login_attempts] = nil

      render inline: "<script>window.top.location = 'https://www.facebook.com/pages/null/#{parsed_signed_request['page']['id']}';</script>"
    else
      # Pass the user through an authentication flow
      # Redirect to OAuth Dialog
      session[:login_attempts] = session[:login_attempts].to_i + 1

      render inline: %Q{
        <script>
          var oauth_url = 'https://www.facebook.com/dialog/oauth/';
          oauth_url += '?client_id=#{ENV['FACEBOOK_APP_ID']}';
          oauth_url += '&redirect_uri=' + encodeURIComponent('https://www.facebook.com/pages/null/#{parsed_signed_request['page']['id']}/?sk=app_#{ENV['FACEBOOK_APP_ID']}');
          oauth_url += '&scope=#{ENV['FACEBOOK_SCOPE']}';

          window.top.location = oauth_url;
        </script>
      }
    end
  end

  private

    def require_like
      render 'facebook/like_gate' unless session[:like]
    end

    def require_authentication
      render status: :bad_request, text: 'You must login through Facebook in order to use this application.' unless logged_in?
    end

    def logged_in?
      current_user != nil
    end

    def current_user
      @current_user ||= User.find_by(user_id: session[:user_id])
    end
end
