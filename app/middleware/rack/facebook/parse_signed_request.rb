module Rack
  module Facebook
    class ParseSignedRequest

      DEFAULT_PATH = '/facebook'

      def initialize(app, path = DEFAULT_PATH)
        @app = app
        @path = path
      end

      def call(env)
        req = Rack::Request.new(env)

        if req.post? && req.path_info == @path
          signed_request = req.POST['signed_request'].to_s
          parsed_signed_request = parse_signed_request(env['facebook.oauth'], signed_request)

          if parsed_signed_request
            logger.info "Successfully parsed the signed request: #{signed_request}"
            logger.info "Parsed signed request information: #{parsed_signed_request}"

            env['facebook.signed_request'] = signed_request
            env['facebook.parsed_signed_request'] = parsed_signed_request
          else
            logger.info "Failed to parse the signed request: \"#{signed_request}\""

            return [403, { 'Content-Type' => 'text/plain' }, ['A valid signed request is required.']]
          end
        end

        @app.call(env)
      end

      private

        def parse_signed_request(oauth, signed_request)
          oauth.parse_signed_request(signed_request)
        rescue Koala::Facebook::OAuthSignatureError
          false
        end

        def logger
          Rails.logger
        end
    end
  end
end
