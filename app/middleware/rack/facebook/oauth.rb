module Rack
  module Facebook
    class Oauth

      def initialize(app, app_id, app_secret)
        @app = app
        @oauth = Koala::Facebook::OAuth.new(app_id, app_secret)
      end

      def call(env)
        env['facebook.oauth'] = @oauth
        @app.call(env)
      end
    end
  end
end
