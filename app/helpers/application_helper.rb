module ApplicationHelper

  def host
    request.env['HTTP_HOST']
  end

  def url_no_scheme(path = '')
    "//#{host}#{path}"
  end
end
